%% Hands on 5
% Not very well coded

% Running sum filters:
syms z h
f1_num = (z^0 + z^1 + z^2 + z^3);
f1_dem = z^3;
factor(f1_num)

r3_num = [1 1 1 1];
r3_dem = [1 0 0 0];
r3_zeros = roots(r3_num);
r3_poles = roots(r3_dem);

r5_num = ones(5+1,1);
r5_dem = [1; zeros(5,1)];
% r5_dem(1) = 1;
% roots(r5_num)

% figure(1)
% zplane(r3_zeros, r3_poles);
figure(2)
freqz(r3_num, r3_dem);
% figure(3)
% freqz(r5_num, r5_dem);

% moving average filters
figure(4)
freqz(r3_num/4, r3_dem);

% The annutated frequencies in the moving average can be enhanced by
% switching the poles and the zeros
%%
% Blocking at the 0.1pi radians.
% exp(0.1pi i) and exp(-0.1pi i)
j=1i;
b_zeros = [exp(0.1*pi*1j); exp(0.1*pi*1j); exp(-0.1*pi*1j); exp(-0.1*pi*1j)];

% Radia of the poles from origo.
R = 0.8;
b_poles = R*b_zeros;

figure(5)
zplane(b_zeros, b_poles);
figure(6)
freqz(poly(b_zeros), poly(b_poles));

%% 1.2

% Low pass at ~0.5 normalized frequency
lp_zeros = 0.5*[exp(0.75j*pi);exp(-0.75j*pi)];
lp_poles = 0.5*[exp(0.25j*pi);exp(-0.25j*pi)];
figure(7)
freqz(poly(lp_zeros),poly(lp_poles))
figure(9)
zplane(lp_zeros,lp_poles);

% High pass, same frq
hp_zeros = lp_poles;
hp_poles = lp_zeros;
figure(8)
freqz(poly(hp_zeros),poly(hp_poles))

% All pass, very rounded
ap_zeros = 0.25*[exp(0.5j*pi);exp(-0.5j*pi);exp(0.25j*pi);exp(-0.25j*pi);exp(0.75j*pi);exp(-0.75j*pi)];
ap_poles = 0.5*[exp(0.5j*pi);exp(-0.5j*pi);exp(0.25j*pi);exp(-0.25j*pi);exp(0.75j*pi);exp(-0.75j*pi)];
figure(10)
freqz(poly(ap_zeros),poly(ap_poles))

%% 2.1 Salami
% load song
[input, Fs] = audioread('piano.wav');

% define filter, here running sum
a = [1 0 0 0];
b = [1 1 1 1];

blocksize = 1024;
% Allocate space
block = zeros(blocksize,1);
boundary = zeros(max(length(a),length(b))-1,1);% For the impulse response~
output = zeros(length(input),1);

% Block processing loop, happens n-1 times, where each k is rounded down.
for k=0:(length(input)/blocksize)-1
    input_buffer = input(1+k*blocksize:(1+k)*blocksize); % Load into buffer
    [output_buffer, boundary] = filter(b,a,input_buffer,boundary); % Filter
    output(1+k*blocksize:(1+k)*blocksize) = output_buffer; % Store in output
    last_k = k; % Pass the end to the straggler handling
end
% Straggler handling
last_part = input(last_k*blocksize:end);
last_output = filter(b,a,last_part,boundary);
output(last_k*blocksize:end) = last_output;

%% test audio

playerObj = audioplayer(input,Fs);
play(playerObj);

%% 3.1 Room sounds

% Load sounds for filtering
[sentence, fs_s] = audioread("spoken_sentence.wav");
[piano, fs_p] = audioread("piano.wav");
impulse = [1];
T0 = length(sentence)/fs_s; % ~ 'bout the same as the sentence
impulse = zero_pad(impulse, fs_s, T0); % Impulse as if sentence


% The fir filter has a = [1]
fir_a = [1]; % Can't leave it empty
alpha = 0.6; % Attenuation coefficient
delay = 200e-3; % 200 ms

tau_s = fs_s * delay; % Sentence version
tau_p = fs_p * delay; % Piano version

% We find the two b vectors, one for each sound
fir_b_s = [1; zeros(tau_s, 1); alpha];
fir_b_p = [1; zeros(tau_p, 1); alpha];

% The iir has something to do with the a, only the system itself.
% [5-30]
iir_a_s = [1; zeros(tau_s, 1); alpha];
iir_a_p = [1; zeros(tau_p, 1); alpha];
iir_b = [1];

%% Play sounds

output_p = filter(iir_b, iir_a_p, piano);

playerObj = audioplayer(output_p ,fs_p);
play(playerObj);

%% Plotting things for the IIR

imp_resp = filter(iir_b, iir_a_s, impulse);
% time vector
t = 0:1/fs_s:T0-1/fs_s;

figure(11)
plot(t,imp_resp)
xlim([0 7*delay]);
title(sprintf("Impulse response of an IIR filter with a delay of %dms", delay*1e3))
xlabel("Time [s]");
ylabel("Amplitude");
saveas(gcf, sprintf("IIR_%dms_impresp.eps", delay*1e3), 'epsc') % Funky save name dependent on delay

% Plot two things together
delay10 = 10e-3; % 10 ms
tau_s10 = fs_s * delay10; % Sentence version
iir_a_s10 = [1; zeros(tau_s10, 1); alpha];

output_s = filter(iir_b, iir_a_s, sentence);
output_s10 = filter(iir_b, iir_a_s10, sentence);

figure(12)
subplot(2,1,1)
plot(t, output_s)
title(sprintf("Sentence filtered with an echo IIR filter with a delay of %dms", delay*1e3))
xlabel("Time [s]");
ylabel("Amplitude");
xlim([0 T0])
subplot(2,1,2)
plot(t, output_s10)
title(sprintf("Sentence filtered with an echo IIR filter with a delay of %dms", delay10*1e3))
xlabel("Time [s]");
ylabel("Amplitude");
xlim([0 T0])
saveas(gcf, sprintf("IIR_%dms_%dms_sentence.eps", delay*1e3, delay10*1e3), 'epsc')

%%
figure(124)
[tmp_y, tmp_f] = make_spectrum(output_s10,fs_s);
plot(tmp_f, tmp_y)

%% Export sounds

output_p = filter(fir_b_p, fir_a, piano);
audiowrite("Piano-fir200ms.wav", output_p, fs_p);
output_s = filter(fir_b_s, fir_a, sentence);
audiowrite("Sentence-fir200ms.wav", output_s, fs_s);

output_p = filter(iir_b, iir_a_p, piano);
audiowrite("Piano-iir200ms.wav", output_p, fs_p);
output_s = filter(iir_b, iir_a_s, sentence);
audiowrite("Sentence-iir200ms.wav", output_s, fs_s);
