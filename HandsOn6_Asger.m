%% Hands on 6

% Making more of less

% We want gain=1 @ omega=0
% min -2dB (G_p=0.785) in the passband, 0<omega<10
% max -11dB (G_s=0.2818) in stopband, 15<omega
% No care for omega > omega_h=35 rad/s

% We choose a fs
fs = 35*3; % 35 rad/s is max input
Ts = 1/fs;
Nf = fs/2; % Nyquist frequency

Wp = 10; % [rad/s]
Ws = 15; % [rad/s]
Rp = -2; % [dB], max ripple in passband
Rs = -11; % [dB], min attenuation

% Normalize things

Wp_norm = Wp/(Nf);
Ws_norm = Ws/(Nf);

[n,Wn] = buttord(Wp_norm, Ws_norm, Rp, Rs); % Find filter cutoff frequency
[z, p, k] = butter(n, Wn); % Recommended digital filter design method.
[b, a] = butter(n, Wn); % Most commonly used filter design method.
% It prewarps, see "doc butter":
% 4. For digital filter design, it uses bilinear to convert the analog ...
% filter into a digital filter through a bilinear transformation with ...
% frequency prewarping. Careful frequency adjustment enables the analog ...
% filters and the digital filters to have the same frequency response ...
% magnitude at Wn or at w1 and w2.

% Look at the digital filter response
figure(1)
freqz(b, a);
title("n=4 low-pass, \omega_p=10rad/s,"+ ...
    " \omega_s=15rad/s, min pass: -2dB, max stop: -11dB");
% saveas(gcf, '11Bode_Normalized.eps', 'epsc');

% Same thing, not normalized
sos = zp2sos(z,p,k); % Example did this, unsure of consequenses.
figure(3)
freqz(sos, 512, fs)
title("n=4 Butterworth, \omega_p=10rad/s, \omega_s=15rad/s," + ...
    " min pass: -2dB, max stop: -11dB");
% saveas(gcf, '11Bode_Not_Normalized.eps', 'epsc');

% Find the transfer function
sys = tf(b,a) % Cont. time tf
% Do a dft instead. --How?

%% Filter a sinusoid with this filter

% Generate a signal
T = 1; % Length in seconds
t = 0:1/fs:T-1/fs; % Time vector
f = 15; % Frequency [rad/s]
signal = sin(2*pi*t*f);

% Filter signal with original filter
filtered_signal = filter(b, a, signal);

% Compare signal and filtered signal
figure(4)
plot(t,signal);
hold on
plot(t,filtered_signal);
legend("Unfiltered signal", "Filtered with original filter");
hold off
% saveas(gcf, '12Filtered_signal.eps', 'epsc');

%% 1.2 Design new filters

% Filter without phase shift by filtering, flipping the signal, and filter.
no_phase_signal = filtfilt(b, a, signal);
% no_phase_signal = flip(filter(b,a,flip(filter(b,a,signal))));

% Plot the unfiltered signal and the filtered one
figure(5)
plot(t, signal);
hold on
plot(t, no_phase_signal);
hold off
xlabel("time [s]")
ylabel("Amplitude")
xlim([0 2])
ylim([-1 1])
legend("Unfiltered signal", "No phase filtered");
title("A sinusoid signal and its filtered signal without phase shift");
% saveas(gcf, '12No_phase_shift.eps', 'epsc');

%% Do a filtering with phase shift, but 2-3x order
% lol,jk, just bode plot the above described thing and all other filters.

% Generate impulse
impulse = [1];
impulse = zero_pad(impulse, fs, T);

% Do all our filters
sf_i_resp = filter(b, a, impulse);
tf_i_resp = filter(b, a, filter(b, a, filter(b, a, impulse)));
nf_i_resp = filtfilt(b, a, impulse);
df_i_resp = filter(b, a, filter(b, a, impulse));

% Do bode plots of filters
[frequency_bins, tf_mag, tf_phase] = bodeFromImpulse(tf_i_resp, fs);
[frequency_bins, sf_mag, sf_phase] = bodeFromImpulse(sf_i_resp, fs);
[frequency_bins, nf_mag, nf_phase] = bodeFromImpulse(nf_i_resp, fs);
[frequency_bins, df_mag, df_phase] = bodeFromImpulse(df_i_resp, fs);

% Show them all in one figure
figure(8)
subplot(2,1,1)
plot(frequency_bins, sf_mag);
hold on
plot(frequency_bins, nf_mag);
plot(frequency_bins, df_mag);
plot(frequency_bins, tf_mag);
hold off
grid on
legend("Original filter", "Filtered, flipped, filtered", ...
    "Filtered two times", "Filtered three times", 'Location', 'northeast');
xlabel("Frequency [Hz]")
ylabel("Magnitude [dB]")
xlim([0 35])
ylim([-50 10])
title("Bode plot of three filters")
subplot(2,1,2)
plot(frequency_bins, sf_phase);
hold on
plot(frequency_bins, nf_phase);
plot(frequency_bins, df_phase);
plot(frequency_bins, tf_phase);
hold off
legend("Original filter", "Filtered, flipped, filtered", ...
    "Filtered two times","Filtered three times", 'Location', 'northeast');
xlim([0 35])
xlabel("Frequency [Hz]")
ylabel("Phase [deg]")
grid on
% saveas(gcf, '12All_Bodeplots.eps', 'epsc');
